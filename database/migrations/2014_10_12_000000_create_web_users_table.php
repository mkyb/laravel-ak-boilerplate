<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('parent_id')->unsigned();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('username')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('secondary_email')->nullable();
            $table->string('mobileno')->nullable();
            $table->string('phoneno')->nullable();

            $table->integer('country_id');
            $table->integer('state_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('city_name')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('gender')->nullable();
            $table->date('dob')->nullable();

            $table->longText('settings')->nullable();
            $table->boolean('active')->default(1);
            $table->boolean('published')->default(1);
            $table->boolean('verified')->default(0);
            $table->boolean('blocked')->default(0);

            $table->string('verification_code');
            $table->mediumText('verification_hash');

            $table->ipAddress('loginip');
            $table->dateTimeTz('lastlogin');

            $table->ipAddress('created_ip');
            $table->ipAddress('updated_ip');

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('web_users');
    }
}
